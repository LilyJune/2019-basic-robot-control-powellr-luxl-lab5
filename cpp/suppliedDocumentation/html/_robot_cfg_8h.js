var _robot_cfg_8h =
[
    [ "ADDR", "_robot_cfg_8h.html#ac9f31f726d2933782e2efda7136a25fd", null ],
    [ "AIN1", "_robot_cfg_8h.html#ab9e303f2afe757487549fb0bc5a7c4fb", null ],
    [ "AIN2", "_robot_cfg_8h.html#add470e0e05c8ce17467a81d7087cba46", null ],
    [ "ALPHABOT2", "_robot_cfg_8h.html#a811b002629a78f3e73a7f49623ab182d", null ],
    [ "BIN1", "_robot_cfg_8h.html#a60e4e11757e695fbf4862a32f377cbd6", null ],
    [ "BIN2", "_robot_cfg_8h.html#ab2c90582798e960f10821c2bda4911df", null ],
    [ "BUZZER", "_robot_cfg_8h.html#a145103118f6d9d1129aa4509cf214a13", null ],
    [ "CHIPSEL", "_robot_cfg_8h.html#aed125283c1de774028bb45902a75b989", null ],
    [ "DL", "_robot_cfg_8h.html#a7c70057f4ac04e167029eef16e7bb770", null ],
    [ "DOUT", "_robot_cfg_8h.html#a0b93cd51079c0fa8e21ee4faf2da1a27", null ],
    [ "DR", "_robot_cfg_8h.html#a349dad5e520932e840e6f8fc806bd94d", null ],
    [ "ENA", "_robot_cfg_8h.html#ac20176fb102e81762b5bb8a08b65a206", null ],
    [ "ENB", "_robot_cfg_8h.html#afb4da94c34e8eeb6ebb83676d92ea36b", null ],
    [ "IOCLK", "_robot_cfg_8h.html#afac4c1f0b886865dc2feb7ee22eb4872", null ]
];