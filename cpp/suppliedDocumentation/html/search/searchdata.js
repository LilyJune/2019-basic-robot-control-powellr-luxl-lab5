var indexSectionsWithContent =
{
  0: "abcdefghiklmnpqrstvwx~",
  1: "cgmnpr",
  2: "cgmnprt",
  3: "cdeghimnprstw~",
  4: "bcefgiklmnpqrstwx",
  5: "dev",
  6: "abflmnprs",
  7: "b"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "defines",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Macros",
  7: "Pages"
};

