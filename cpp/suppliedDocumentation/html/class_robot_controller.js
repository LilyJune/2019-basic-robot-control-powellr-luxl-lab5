var class_robot_controller =
[
    [ "RobotController", "class_robot_controller.html#a5f3416eeaf0fd3d1af08d2eef2e21d82", null ],
    [ "~RobotController", "class_robot_controller.html#a4441cd6adf323a0ce3c454dc4f02efaf", null ],
    [ "processMotionControlCommand", "class_robot_controller.html#a76d37b76b29d19ed5e50e59cb5cc1b3c", null ],
    [ "processSpeedControlCommand", "class_robot_controller.html#a19be18bfd3e2000acc486ae686dbafbc", null ],
    [ "run", "class_robot_controller.html#a6ae29d03e20d5b24e4b83a43f14602a2", null ],
    [ "stop", "class_robot_controller.html#a122b64b7cf020a75798e78d35e2b204b", null ],
    [ "currentOperation", "class_robot_controller.html#aebb93b4a9755754f3c4ebb6abb2ce86a", null ],
    [ "currentSpeed", "class_robot_controller.html#ad78a6575408c6cea2feabb34e6e6f0ab", null ],
    [ "leftMotor", "class_robot_controller.html#aab0453e0c10ad840a8c33b3c99aa4e62", null ],
    [ "referencequeue", "class_robot_controller.html#a1849d53b60abeef15acb2036eaacbc28", null ],
    [ "rightMotor", "class_robot_controller.html#ab700a59301dc5a12caf84d4c16f38778", null ]
];