#ifndef MOTORCONTROLLER_H
#define MOTORCONTROLLER_H

#include "GPIO.h"
#include "PWMManager.h"

class MotorController {
private:
	se3910RPi::GPIO* forwardIOPin;
	se3910RPi::GPIO* backwardIOPin;
	se3910RPi::GPIO* enablePin;
	int speed;
	PWMManager& managerInstance;

public:
	MotorController(int forwardPinNumber, int backwardPinNumber, int enablePinNumber, PWMManager& manager);
	~MotorController();
	void setSpeed(int);
	void setDirection(int);
	void stop();
};

#endif
