/*
 * NetworkManager.h
 *
 *  Created on: Oct 2, 2019
 *      Author: se3910
 */

#ifndef NETWORKMANAGER_H_
#define NETWORKMANAGER_H_

#include "CommandQueue.h"
#include "RunnableClass.h"
#include <string.h>

class NetworkManager : public RunnableClass {
private:
	int port;
	CommandQueue** referenceQueue;
	int server_fd = 0;

public:
	NetworkManager(int port, CommandQueue* commands[], std::string threadname);
	~NetworkManager();
	void run();
//	void stop();
};

#endif /* NETWORKMANAGER_H_ */
