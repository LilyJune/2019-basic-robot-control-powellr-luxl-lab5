/**
 *
 */

#ifndef PWMMANAGER_H_
#define PWMMANAGER_H_

#include <map>
#include "GPIO.h"
#include "PeriodicTask.h"


/**
 *
 */
class PWMManager : public PeriodicTask{

private:

	/**
	 *
	 */
	struct GPIOPWMControlMapStruct {
			se3910RPi::GPIO* gpioPin;
			uint32_t dutyCycle;
			bool changed;
		};

	/**
	 *
	 */
	std::map<int, GPIOPWMControlMapStruct> GPIOControlMap;

	/**
	 *
	 */
	uint32_t currentCount;

public:


	/**
	 *
	 */
	PWMManager(std::string, uint32_t);

	/**
	 *
	 */
	~PWMManager();

	/**
	 *
	 */
	void addPWMPin(int, se3910RPi::GPIO*, uint32_t);

	/**
	 *
	 */
	void removePWM(int);

	/**
	 *
	 */
	void setDutyCycle(int, uint32_t);

	/**
	 *
	 */
	void taskMethod();
};

#endif
