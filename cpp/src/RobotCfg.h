/**
 * @file RobotCfg.h
 * @author  Walter Schilling (schilling@msoe.edu)
 * @version 1.0
 *
 * @section LICENSE
 *
 * This code is developed as part of the MSOE SE3910 Real Time Systems course,
 * but can be freely used by others.
 *
 * SE3910 Real Time Systems is a required course for students studying the
 * discipline of software engineering.
 *
 * This Software is provided under the License on an "AS IS" basis and
 * without warranties of any kind concerning the Software, including
 * without limitation merchantability, fitness for a particular purpose,
 * absence of defects or errors, accuracy, and non-infringement of
 * intellectual property rights other than copyright. This disclaimer
 * of warranty is an essential part of the License and a condition for
 * the grant of any rights to this Software.
 *
 * @section DESCRIPTION
 *      This file allows us to easily configure the robot for different IO setups. 
 */

#ifndef ROBOTCFG_H_
#define ROBOTCFG_H_

#define ALPHABOT2

/**
 * The following definitions are for the ALPHABOT.
 */
#ifdef ALPHABOT
#define AIN1 (12)
#define AIN2 (13)
#define ENA (6)
#define ENB (26)
#define IN3 (20)
#define IN4 (21)
#define DL (16)
#define DR (19)

#endif

/**
 * The following definitions are for the ALPHABOT2.  Basically, it defines the GPIO pin for various items.
 */
#ifdef ALPHABOT2
#define AIN1 (12)
#define AIN2 (13)
#define ENA (6)
#define ENB (26)
#define BIN1 (20)
#define BIN2 (21)
#define DL (16)
#define DR (19)
#define BUZZER (4)
#define IOCLK (25)
#define ADDR (24)
#define DOUT (23)
#define CHIPSEL (5)

#endif
#endif /* ROBOTCFG_H_ */
